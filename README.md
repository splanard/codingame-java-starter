# codingame-java-starter

> A starter project to solve CodinGame puzzles and challenges as a standard Java project

## Puzzles

To solve puzzles, please checkout the branch [`puzzle`](https://gitlab.com/splanard/codingame-java-starter/-/tree/puzzle).

## Multi-turn games

To play multi-turn games (some puzzles, optimization games, challenges and bot programming games), please checkout the branch [`multiturn-game`](https://gitlab.com/splanard/codingame-java-starter/-/tree/multiturn-game).
